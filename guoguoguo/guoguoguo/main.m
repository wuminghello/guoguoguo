//
//  main.m
//  guoguoguo
//
//  Created by wuming on 14-9-16.
//  Copyright (c) 2014年 wuming. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
