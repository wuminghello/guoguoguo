//
//  AppDelegate.h
//  guoguoguo
//
//  Created by wuming on 14-9-16.
//  Copyright (c) 2014年 wuming. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
