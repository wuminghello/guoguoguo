//
//  GGNetworkHandle.h
//  guoguoguo
//
//  Created by wuming on 14-9-16.
//  Copyright (c) 2014年 wuming. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^GG_API_Request_Callback)(BOOL isSuccess,NSDictionary *result,NSError **error);



typedef enum _GG_API_request_method {
    GG_API_request_method_get = 1,
    GG_API_request_method_post = 2,
} GG_API_request_method;

typedef enum _GG_API_request_protocol {
    GG_API_request_protocol_http = 1,
    GG_API_request_protocol_https = 2,
} GG_API_request_protocol;

@interface GGNetworkHandle : NSObject


- (void)startRequestWithmethod:(GG_API_request_method)method
               requestProtocol:(GG_API_request_protocol)protocol
                        isAsyc:(BOOL)isAsyc
                        params:(NSDictionary *)params
                    completion:(GG_API_Request_Callback)callback;


@end
